context.tipopergunta.AddOrUpdate(p => p.tipo, 
                                             new TipoPergunta { tipo = "Texto", nopcoes = 4 },
                                             new TipoPergunta { tipo = "Video", nopcoes = 4},
                                             new TipoPergunta { tipo = "AUdio", nopcoes = 4},
                                             new TipoPergunta { tipo = "Verdadeiro/Falso", nopcoes = 2});
            context.pais.AddOrUpdate(
                p => p.nome,
                new Pais { nome = "Australia", perguntas = new List<Pergunta> {
                    new Pergunta { pergunta = "Which Australian city hosted its final Formula 1 race in 1995?", 
                                    correta = new Resposta {resposta = "Adelaide"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "Atayde"},
                                               new Resposta{ resposta = "Rita"},
                                               new Resposta{ resposta = "Jessica"}},                
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Texto")}
                    } ,
                    new Pergunta { pergunta = "Which is the nearest major town to the southwest of Ayers Rock?", 
                                    correta = new Resposta {resposta = "Alice Springs"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "Julia Roberts"},
                                               new Resposta{ resposta = "Jessica Springs"},
                                               new Resposta{ resposta = "Ruth Hash"}},                
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Texto")}
                    } ,
                    new Pergunta { pergunta = "Is the great victoria desert located in Australia?", 
                                    correta = new Resposta {resposta = "True"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "False"}},              
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Verdadeiro/Falso")}
                    } ,
                    new Pergunta { pergunta = "Australia has a Continent name?", 
                                    correta = new Resposta {resposta = "True"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "False"}},              
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Verdadeiro/Falso")}
                    } ,
                    new Pergunta { pergunta = "What and when is the biggest national celebration every year in Australia?", 
                                    correta = new Resposta {resposta = "Australia Day"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "Independence Day"},
                                               new Resposta{ resposta = "Mother's Day"},
                                               new Resposta{ resposta = "Father's Day"}},                
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Texto")}
                    } ,
                    new Pergunta { pergunta = "Which sacred rock is the world's largest monolith?", 
                                    correta = new Resposta {resposta = "Ayers Rock"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "Fraser Island"},
                                               new Resposta{ resposta = "Mount Bartle Frere"},
                                               new Resposta{ resposta = "Brisbane river"}},                
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Texto")}
                    } ,
                    new Pergunta { pergunta = "What is the name of the surfing beach on the outskirts of Sydney?", 
                                    correta = new Resposta {resposta = "Bondi Beach"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "Botany Bay"},
                                               new Resposta{ resposta = "Brisbane river"},
                                               new Resposta{ resposta = "Great Barrier Reef"}},                
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Texto")}
                    } ,
                    new Pergunta { pergunta = "At which famous east coast bay did James Cook arrive in April 1770?", 
                                    correta = new Resposta {resposta = "Botany Bay"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "Bondi Beach"},
                                               new Resposta{ resposta = "Brisbane river"},
                                               new Resposta{ resposta = "Great Barrier Reef"}},                
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Texto")}
                    } ,
                    new Pergunta { pergunta = "Which meandering river is Brisbane built around?", 
                                    correta = new Resposta {resposta = "Brisbane river"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "Bondi Beach"},
                                               new Resposta{ resposta = "Botany Bay"},
                                               new Resposta{ resposta = "Great Dividing Range"}},                
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Texto")}
                    } ,
                    new Pergunta { pergunta = "What is Queensland's most northerly city?", 
                                    correta = new Resposta {resposta = "Cairns"},
                                    erradas = new List<Resposta> {
                                               new Resposta{ resposta = "Mount Bartle Frere"},
                                               new Resposta{ resposta = "Grampians"},
                                               new Resposta{ resposta = "Kuranda, Queensland"}},                
                                    tipo = context.tipopergunta.ToList().Find(x => x.tipo == "Texto")}
                    } 

                }
                