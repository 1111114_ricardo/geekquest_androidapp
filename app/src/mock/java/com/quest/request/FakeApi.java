package com.quest.request;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.quest.model.Question;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

class FakeApi implements ApiClientService {

    @Override
    public void getSetOfQuestions(@NonNull Response<List<Question>> callback, @NonNull String region) {
        try {
            Gson gson = new Gson();
            InputStream stream = getClass().getClassLoader().getResourceAsStream("res/raw/questions.json");
            JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
            List<Question> data = gson.fromJson(reader, new TypeToken<List<Question>>() {}.getType());
            callback.onSuccess(data);
        } catch (UnsupportedEncodingException e) {
            callback.onError(null);
        }
    }
}
