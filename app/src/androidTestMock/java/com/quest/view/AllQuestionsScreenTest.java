package com.quest.view;

import android.app.Instrumentation;
import android.support.annotation.IdRes;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.quest.R;
import com.quest.ToastMatcher;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.quest.Utils.withBackgroundColor;


@RunWith(AndroidJUnit4.class)
public class AllQuestionsScreenTest {

    @Rule
    public IntentsTestRule<AllQuestionsActivity> questionTestRule =
            new IntentsTestRule<>(AllQuestionsActivity.class);


    @Test
    public void clickQuestionAndGetsItRight(){
        checkQuestionRight(R.id.button1);
    }

    @Test
    public void clickQuestionAndGetsItWrong(){
        intending(hasComponent(QuestionActivity.class.getName())).
                respondWith(new Instrumentation.ActivityResult(AllQuestionsActivity.RESULT_WRONG,null));
        onView(withId(R.id.button1)).perform(click());
        onView(withId(R.id.button1)).check(matches(withBackgroundColor(R.color.red)));
    }

    @Ignore
    @Test
    public void clickQuestionAndGetsAnError(){
        intending(hasComponent(QuestionActivity.class.getName())).
                respondWith(new Instrumentation.ActivityResult(AllQuestionsActivity.RESULT_ERROR,null));
        onView(withId(R.id.button1)).perform(click());
//        checkToastInActivity(R.string.result_error, questionTestRule);
        onView(withText(R.string.result_error)).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    @Ignore
    @Test
    public void answerAllQuestionsRightAndWinsTheGame(){
        int[] buttons = {R.id.button1,R.id.button2,R.id.button3,R.id.button4,R.id.button5,
                R.id.button6,R.id.button7,R.id.button8,R.id.button9,R.id.button10 };

        for(int button: buttons){
            checkQuestionRight(button);
        }
//        checkToastInActivity(R.string.won_knowledge_game, questionTestRule);
        onView(withText(R.string.won_knowledge_game)).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    private void checkQuestionRight(@IdRes final int id){
        intending(hasComponent(QuestionActivity.class.getName())).
                respondWith(new Instrumentation.ActivityResult(AllQuestionsActivity.RESULT_RIGHT,null));
        onView(withId(id)).perform(click());
        onView(withId(id)).check(matches(withBackgroundColor(R.color.green)));
    }

}
