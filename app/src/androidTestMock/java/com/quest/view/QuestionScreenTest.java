package com.quest.view;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.quest.R;
import com.quest.controller.QuestionForGame;
import com.quest.controller.QuestionsController;
import com.quest.request.ApiClientService;

import junit.framework.Assert;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.quest.R.id.phone;
import static com.quest.view.Utils.isEmpty;
import static junit.framework.Assert.fail;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class QuestionScreenTest {

    @Rule
    public IntentsTestRule<QuestionActivity> questionTestRule =
            new IntentsTestRule<>(QuestionActivity.class, true, false);


    @Before
    public void init(){
        //TODO implement getting only one question
        new QuestionsController().getQuestions(new ApiClientService.Response<List<QuestionForGame>>() {
            @Override
            public void onSuccess(List<QuestionForGame> ob) {
                if(!isEmpty(ob)){
                    final QuestionForGame question = ob.get(0);
                    Intent intent = new Intent();
                    intent.putExtra(AllQuestionsActivity.QUESTION_BUNDLE, question);
                    questionTestRule.launchActivity(intent);
                } else {
                    onError(new IllegalStateException());
                }
            }

            @Override
            public void onError(Throwable t) {
                fail();
            }
        }, "Portugal");
    }

    @Test
    public void clickFiftyFiftyButtonAndShowsHalfOfQuestions(){
        onView(withText(R.string.half_of_questions)).perform(click());
        int[] buttons = {R.id.answer1Button, R.id.answer2Button, R.id.answer3Button, R.id.answer4Button};

        int count = 0;
        for(int button: buttons){
            View view = questionTestRule.getActivity().findViewById(button);
            if(view.getVisibility() == View.GONE){
                count++;
            }
        }
        Assert.assertEquals(2, count);
    }

    @Test
    public void clickPhoneIconAndMakesCall() throws RemoteException, OperationApplicationException {

        Matcher<Intent> contactPicker = allOf(hasData(ContactsContract.CommonDataKinds.Phone.CONTENT_URI), hasAction(Intent.ACTION_PICK));
        intending(contactPicker).
                respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, createContactPickerReturnIntent()));

        Matcher<Intent> phoneCall = hasAction(Intent.ACTION_CALL);
        intending(phoneCall).
                respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));


        onView(withId(phone)).perform(click());

        onView(withId(R.id.phone)).check(doesNotExist());

        intended(contactPicker);
        intended(phoneCall);

    }

    @NonNull
    private Intent createContactPickerReturnIntent() throws RemoteException, OperationApplicationException, NullPointerException {

        ArrayList<ContentProviderOperation> ops =
                new ArrayList<>();

        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, "Vikas Patidar") // Name of the person
                .build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(
                        ContactsContract.Data.RAW_CONTACT_ID,   0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, "9999999999") // Number of the person
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build()); // Type of mobile number

        ContentProviderResult[] res = questionTestRule.getActivity().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        Uri newContactUri = res[2].uri;
        Intent intent = new Intent();
        intent.setData(newContactUri);

        return intent;

    }
}
