package com.quest.request;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.GsonBuilder;
import com.quest.R;
import com.quest.model.Question;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class BaqGameAPIClient implements ApiClientService {

    private static BaqGameServices service;

    BaqGameAPIClient(@NonNull Context context){
        if(service == null) {
            initService(context);
        }
    }

    private void initService(@NonNull Context context){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.networkInterceptors().add(httpLoggingInterceptor);
        builder.addInterceptor(new JsonInterceptor());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getResources().getString(R.string.api_url))
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .client(builder.build())
                .build();
        service = retrofit.create(BaqGameServices.class);
    }

    /**
     *  Converts the data returned from data loader to String.
     *
     * @return The String with the objects to be parsed.
     */
    protected String entityToString(Object messageEntity) throws IOException {

//        StringBuilder response = new StringBuilder();
//
//        InputStream is = messageEntity.getContent();
//        BufferedReader br = new BufferedReader(new InputStreamReader(is));
//        String line;
//        while ((line = br.readLine()) != null) {
//            response.append(line);
//        }
//        return response.toString();
        return null;
    }

    public void getSetOfQuestions(@NonNull final ApiClientService.Response<List<Question>> callback, @NonNull String region) {
        Call<List<Question>> call = service.getSetOfQuestions(region);
        call.enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, retrofit2.Response<List<Question>> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable t) {
                callback.onError(t);
            }
        });

    }

    public boolean suggestQuestion(String question) {
//        HttpResponse httpResponse = dataloader.secureLoadDataPost(remoteAddress + suggestedQuestions, question);
//
//        int statusCode = httpResponse.getStatusLine().getStatusCode();
//
//        if(statusCode != HttpURLConnection.HTTP_CREATED)
//            throw new ErrorLoadingDataException(statusCode+"");
//
        return true;
    }

    public String getSuggestedQuestions() {
//        HttpResponse httpResponse = dataloader.secureLoadDataGet(remoteAddress + suggestedQuestions);
//
//        int statusCode = httpResponse.getStatusLine().getStatusCode();
//
//        if(statusCode != HttpURLConnection.HTTP_OK)
//            throw new ErrorLoadingDataException(statusCode+"");
//
//        return entityToString(httpResponse.getEntity());
        return null;
    }

    public boolean voteQuestion(int id, double latitude, double longitude) {
//        HttpResponse httpResponse = dataloader.secureLoadDataPost(remoteAddress + vote +
//                sQuestionToVote + id +voteLatitude + latitude + voteLongitude + longitude);
//
//        int statusCode = httpResponse.getStatusLine().getStatusCode();
//
//        if(statusCode != HttpURLConnection.HTTP_ACCEPTED)
//            throw new ErrorLoadingDataException(statusCode+"");

        return true;
    }
}
