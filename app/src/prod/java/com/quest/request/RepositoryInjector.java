package com.quest.request;


import com.quest.view.MyApplication;

public class RepositoryInjector {

    public ApiClientService get(){
        return new BaqGameAPIClient(MyApplication.getContext());
    }

}
