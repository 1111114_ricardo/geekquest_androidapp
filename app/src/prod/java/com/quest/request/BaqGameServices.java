package com.quest.request;

import com.quest.model.Question;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

interface BaqGameServices {

    @Headers("Accept-Language: en")
    @GET("questions/quest")
    Call<List<Question>> getSetOfQuestions(@Query("Region") String region);

}
