package com.quest.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.quest.view.AllQuestionsActivity;
import com.quest.view.Utils;

import java.io.Serializable;

import static com.quest.view.Utils.checkNotNull;

public class QuestionController implements QuestionContract.Controller {

    private QuestionContract.View view;
    @Nullable
    private QuestionForGame question;

    public QuestionController(@NonNull QuestionContract.View view) {
        this.view = view;
    }

    @Override
    public void onHalfQuestionsClick() {
        view.setHalfOfQuestions(checkNotNull(question));
        AllQuestionsActivity.setUsedHalfQuestions(true);
    }

    @Override
    public void onPhoneClick() {
        view.startContactPicker();
        AllQuestionsActivity.setUsedPhone(true);
    }

    @Override
    public void onRightAnswer() {
        view.sendResultRight();
    }

    @Override
    public void onWrongAnswer() {
        view.sendResultWrong();
    }

    @Override
    public void onQuestionError() {
        view.sendQuestionError();
    }

    @Override
    public void onPhoneNumberGotten(@Nullable String phoneNumber) {
        if(Utils.isNotEmpty(phoneNumber)){
            view.makePhoneCall(phoneNumber);
        } else {
            onUnexpectedError();
        }
    }

    @Override
    public void onUnexpectedError() {
        view.showUnexpectedErrorDialog();
    }

    @Override
    public void onCreateView(@Nullable Bundle bundle) {
        if(bundle == null) {
            onQuestionError();
        } else {
            Serializable serializable = bundle.getSerializable(AllQuestionsActivity.QUESTION_BUNDLE);
            if(serializable instanceof QuestionForGame){
                question = (QuestionForGame) serializable;
                view.configLayout(question);
            } else {
                onQuestionError();
            }
        }
    }
}
