package com.quest.controller.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quest.R;

import java.util.List;

public class ConnectionsAdapter extends RecyclerView.Adapter<ConnectionsAdapter.ConnectionViewHolder> {

    private List<String> connections;

    private OnItemClick onItemClick;

    public interface OnItemClick {
        public void onItemClicked(int position, String connection);
    }

    public ConnectionsAdapter(@NonNull List<String> connections, @NonNull OnItemClick onItemClick) {
        this.connections = connections;
        this.onItemClick = onItemClick;
    }

    public class ConnectionViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        TextView connection;

        public ConnectionViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            connection = (TextView) itemView.findViewById(R.id.connection_text_view);
        }
    }

    @Override
    public ConnectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.connection_holder, parent, false);

        return new ConnectionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ConnectionViewHolder holder, int position) {
        final int positionOfConnection = position;
        final String connection = connections.get(position);
        holder.connection.setText(connection);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItemClicked(positionOfConnection, connection);
            }
        });
    }

    @Override
    public int getItemCount() {
        return connections.size();
    }

    public void addConnection(@NonNull String connection) {
        this.connections.add(connection);
    }
}
