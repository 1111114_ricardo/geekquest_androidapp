package com.quest.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

public interface ChallengeContract {

    interface View {
        void startAllQuestionsActivity(@NonNull final List<QuestionForGame> questions);

        void gotNewConnection(@NonNull String connection);

        void buildRecyclerView(@Nullable List<String> connections);

        void showConnectionError();

        void end();
    }

    interface Controller {
        void connect(@NonNull final String connection);

        void onViewCompletelyLoaded();

        void onBuildView(@Nullable Bundle bundle);
    }

}
