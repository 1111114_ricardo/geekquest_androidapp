package com.quest.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface QuestionContract {

    interface View{
        void sendResultRight();
        void sendResultWrong();
        void sendQuestionError();
        void startContactPicker();
        void makePhoneCall(@NonNull final String phoneNumber);
        void setHalfOfQuestions(@NonNull QuestionForGame question);
        void configLayout(@NonNull QuestionForGame question);
        void showUnexpectedErrorDialog();
    }

    interface Controller{
        void onHalfQuestionsClick();
        void onPhoneClick();
        void onRightAnswer();
        void onWrongAnswer();
        void onQuestionError();
        void onPhoneNumberGotten(@Nullable final String phoneNumber);
        void onUnexpectedError();
        void onCreateView(@Nullable Bundle bundle);
    }
}
