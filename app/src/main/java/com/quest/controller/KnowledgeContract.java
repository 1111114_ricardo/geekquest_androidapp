package com.quest.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface KnowledgeContract {
    interface View{
        void showLoadingQuestionsDialog();
        void dismissLoadingQuestionsDialog();
        void showToastErrorLoadingQuestions();
        void setListenerForButton(final int currentQuestion);
        void setNullListenerForButton(final int currentQuestion);
        void startQuestionActivity(@NonNull QuestionForGame question);
        void currentQuestionRightBackground(final int currentQuestion);
        void currentQuestionWrongBackground(final int currentQuestion);
        void showWonGameToast();

        void showLostGameToast();
        void showSimpleError();

        void showConnectionError();

        void finish();
    }

    interface Controller{
        void onCreateView(@Nullable Bundle bundle);
        void getQuestions();
        void rightAnswer();
        void wrongAnswer();
        void errorQuestion();
        void configCurrentButton();
        void onClickCurrentQuestion();

        void backPressed();
    }
}
