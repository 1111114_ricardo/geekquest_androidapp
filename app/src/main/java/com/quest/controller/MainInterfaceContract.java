package com.quest.controller;

public interface MainInterfaceContract {

    interface View{
        void startAllQuestionsActivity(String country);

        void startChallengeActivity(String country);
        void startReceiveChallengeActivity();

        void startBluetoothActivity();
    }

    interface Controller{
        void onClickTestKnowledgeButton();
        void onClickChallengeButton();
        void onClickReceiveChallengeButton();

        void onGotCountry(String country);

        void onClickBluetoothButton();
    }
}
