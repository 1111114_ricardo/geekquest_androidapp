package com.quest.controller;

import android.support.annotation.NonNull;

import com.quest.communication.IDataTransitorCommunication;
import com.quest.communication.MasterService;
import com.quest.communication.QuestionsList;
import com.quest.communication.TransitorData;
import com.quest.communication.UpdatesService;
import com.quest.communication.sockets.SocketMaster;

public class BeChallengedController implements BeChallengedContract.Controller, UpdatesService {

    private BeChallengedContract.View view;

    private MasterService masterService;

    private boolean playing;

    public BeChallengedController(@NonNull BeChallengedContract.View view) {
        this.view = view;
    }

    public void listenForChallenges() {
        masterService = new SocketMaster(this);

        masterService.listen();
    }

    @Override
    public void viewCompletelyLoaded() {
        if (playing) {
            gameFinished();
        }
    }

    @Override
    public void gameFinished() {
        view.end();
        masterService.stopListen();
        masterService.setUpdatesService(null);
    }

    @Override
    public void gotUpdate(@NonNull Object data) {
        if (data instanceof TransitorData) {
            TransitorData transitorData = (TransitorData) data;
            if (transitorData.type == TransitorData.QUESTIONS && transitorData.data instanceof QuestionsList) {
                IDataTransitorCommunication.setInstance(masterService);
                QuestionSingleton.getInstance().clear();
                QuestionSingleton.getInstance().addAll((QuestionsList) transitorData.data);
                playing = true;
                view.startAllQuestionsActivity((QuestionsList) transitorData.data);
            }
        }
    }

    @Override
    public void gotError(Throwable throwable) {
        view.showConnectionError();
    }
}
