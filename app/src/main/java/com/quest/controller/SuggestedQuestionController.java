package com.quest.controller;

import com.quest.request.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class SuggestedQuestionController {

    public boolean suggestQuestion(String question, int difficulty, String answer1, String answer2, String answer3, String answer4) throws JSONException {
        JSONObject jsonSuggestedQuestion = new JSONObject();
        jsonSuggestedQuestion.put(Constants.DESCRIPTION,question);

        JSONObject jsonAnswer1 = new JSONObject();
        JSONObject jsonAnswer2 = new JSONObject();
        JSONObject jsonAnswer3 = new JSONObject();
        JSONObject jsonAnswer4 = new JSONObject();
        jsonAnswer1.put(Constants.DESCRIPTION,answer1);
        jsonAnswer1.put(Constants.CORRECT,true);
        jsonAnswer2.put(Constants.DESCRIPTION,answer2);
        jsonAnswer3.put(Constants.DESCRIPTION,answer3);
        jsonAnswer4.put(Constants.DESCRIPTION,answer4);
        JSONArray jsonAnswers = new JSONArray();
        jsonAnswers.put(jsonAnswer1);
        jsonAnswers.put(jsonAnswer2);
        jsonAnswers.put(jsonAnswer3);
        jsonAnswers.put(jsonAnswer4);


        jsonSuggestedQuestion.put(Constants.ANSWERS,jsonAnswers);
        jsonSuggestedQuestion.put(Constants.DIFFICULTY, difficulty);


//        return new BaqGameAPIClient().suggestQuestion(jsonSuggestedQuestion.toString());
        return false;
    }

    public List<QuestionForGame> getSuggestedQuestions() throws JSONException {
//        List<QuestionForGame> instance = new LinkedList<>();
//        String json = new BaqGameAPIClient().getSuggestedQuestions();
//        JSONArray jsonArray = new JSONArray(json);

//        for(int i = 0; i<jsonArray.length();i++) {
//            JSONObject jsonQuestion = jsonArray.getJSONObject(i);
//            QuestionForGame questionForExample = new QuestionForGame(jsonQuestion);
//            instance.add(questionForExample);
//        }
        return null;
    }

    public boolean voteSuggestedQuestion(int id, double lat, double lng) {
//        return new BaqGameAPIClient().voteQuestion(id,lat,lng);
        return false;
    }
}
