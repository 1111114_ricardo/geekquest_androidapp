package com.quest.controller;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.quest.model.Question;
import com.quest.request.ApiClientService;
import com.quest.request.RepositoryInjector;

import java.util.ArrayList;
import java.util.List;


public class QuestionsController {

    public QuestionsController(){

    }

    public void getQuestions(@NonNull final ApiClientService.Response<List<QuestionForGame>> response, @NonNull String region) {

        new RepositoryInjector().get().getSetOfQuestions(new ApiClientService.Response<List<Question>>() {
            @Override
            public void onSuccess(List<Question> questions) {
                synchronized (QuestionSingleton.getInstance()) {
                    List<QuestionForGame> singleton = QuestionSingleton.getInstance();
                    singleton.clear();
                    for (Question question : questions) {
                        singleton.add(new QuestionForGame(question));
                    }
                }
                response.onSuccess(QuestionSingleton.getInstance());
            }

            @Override
            public void onError(Throwable t) {
                response.onError(t);
            }
        }, region);
    }

    @Nullable
    public List<QuestionForGame> getQuestionsOfLevel(int level){
        List<QuestionForGame> questionsOfLevel = null;
        synchronized (QuestionSingleton.getInstance()) {
            for(QuestionForGame questionForGame : QuestionSingleton.getInstance()){
                if(questionForGame.question.getDifficulty() == level){
                    if(questionsOfLevel == null){
                        questionsOfLevel = new ArrayList<>();
                    }
                    questionsOfLevel.add(questionForGame);
                }
            }
        }
        return questionsOfLevel;
    }
}
