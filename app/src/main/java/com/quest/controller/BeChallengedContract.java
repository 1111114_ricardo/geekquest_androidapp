package com.quest.controller;

import android.support.annotation.NonNull;

import java.util.List;

public interface BeChallengedContract {

    interface View {
        void startAllQuestionsActivity(@NonNull final List<QuestionForGame> questions);

        void showConnectionError();

        void end();
    }

    interface Controller {
        void listenForChallenges();

        void viewCompletelyLoaded();

        void gameFinished();
    }

}
