package com.quest.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.quest.communication.ConnectionService;
import com.quest.communication.IDataTransitorCommunication;
import com.quest.communication.QuestionsList;
import com.quest.communication.SlaveService;
import com.quest.communication.TransitorData;
import com.quest.communication.sockets.SocketSlave;
import com.quest.request.ApiClientService;

import java.util.List;

public class ChallengeController implements ChallengeContract.Controller, ConnectionService {

    private SlaveService slave;

    private ChallengeContract.View view;

    @Nullable
    private QuestionsList questions;

    private boolean connected;

    private String country;

    public ChallengeController(@NonNull ChallengeContract.View view) {
        slave = new SocketSlave(this);
        this.view = view;
    }

    @Override
    public void connect(@NonNull final String connection) {
        if (!connected) {
            if (questions == null) {
                new QuestionsController().getQuestions(new ApiClientService.Response<List<QuestionForGame>>() {
                    @Override
                    public void onSuccess(List<QuestionForGame> ob) {
                        addToList(ob);
                        slave.connect(connection);
                    }

                    @Override
                    public void onError(Throwable t) {
                        view.showConnectionError();
                    }

                }, this.country);
            } else {
                slave.connect(connection);
            }
        }
    }

    private void addToList(@NonNull List<QuestionForGame> questionForGames) {
        if (questions == null) {
            questions = new QuestionsList();
        }
        questions.addAll(questionForGames);
    }

    @Override
    public void onNewConnection(@NonNull String connection) {
        view.gotNewConnection(connection);
    }

    @Override
    public void onConnectionOpen() {
        connected = true;
        if (questions != null) {
            slave.sendData(new TransitorData(TransitorData.QUESTIONS, questions));
            IDataTransitorCommunication.setInstance(slave);
            view.startAllQuestionsActivity(questions);
        }
    }

    @Override
    public void onViewCompletelyLoaded() {
        if (connected) {
            gameFinished();
        }
    }

    private void gameFinished() {
        slave.disconnect();
        slave.setUpdatesService(null);
        view.end();
    }

    @Override
    public void onBuildView(@Nullable Bundle bundle) {
        if (bundle != null && bundle.containsKey("Country")) {
            this.country = bundle.getString("Country");
            view.buildRecyclerView(slave.getConnections());
        } else {
            view.showConnectionError();
        }
    }
}
