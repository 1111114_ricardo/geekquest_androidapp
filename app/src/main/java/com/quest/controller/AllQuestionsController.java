package com.quest.controller;

import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.quest.communication.IDataTransitor;
import com.quest.communication.IDataTransitorCommunication;
import com.quest.communication.TransitorData;
import com.quest.communication.UpdatesService;
import com.quest.request.ApiClientService;
import com.quest.view.AllQuestionsActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.quest.view.Utils.isEmpty;


public class AllQuestionsController implements KnowledgeContract.Controller, UpdatesService {

    public static final int NUMBER_OF_QUESTIONS = 10;

    public static final int LEVEL_1 = 1;
    public static final int LEVEL_2 = 2;
    public static final int LEVEL_3 = 3;
    public static final int LEVEL_4 = 4;

    private QuestionsController questionsController;

    private KnowledgeContract.View view;

    private int currentQuestion;

    private List<Integer> questionsTaken;
    private String mCountry;

    @Nullable
    private IDataTransitor dataTransitor;

    public AllQuestionsController(@NonNull KnowledgeContract.View view) {
        this.view = view;
        questionsController = new QuestionsController();
        questionsTaken = new ArrayList<>();
        currentQuestion = 0;
    }

    @Override
    public void onCreateView(@Nullable Bundle bundle) {
        if (bundle != null) {
            String country = bundle.getString("Country");
            if (bundle.containsKey(AllQuestionsActivity.MULTIPLAYER)) {
                IDataTransitor transitor = IDataTransitorCommunication.getInstance();
                if (transitor != null) {
                    dataTransitor = transitor;
                    dataTransitor.setUpdatesService(this);
                    view.setListenerForButton(currentQuestion);
                } else {
                    view.showToastErrorLoadingQuestions();
                }
            } else if (country != null) {
                this.mCountry = country;
                getQuestions();

            }
        } else {
            view.showToastErrorLoadingQuestions();
        }
    }

    @Override
    public void getQuestions() {
        view.showLoadingQuestionsDialog();
        questionsController.getQuestions(new ApiClientService.Response<List<QuestionForGame>>() {
            @Override
            public void onSuccess(List<QuestionForGame> ob) {
                view.dismissLoadingQuestionsDialog();
                view.setListenerForButton(currentQuestion);
            }

            @Override
            public void onError(Throwable t) {
                view.dismissLoadingQuestionsDialog();
                view.showToastErrorLoadingQuestions();
            }
        }, mCountry);
    }

    @Override
    public void rightAnswer() {
        view.currentQuestionRightBackground(currentQuestion);
        view.setNullListenerForButton(currentQuestion);
        currentQuestion++;
        if (!checkIfWon()) {
            configCurrentButton();
        } else {
            sendWonGame();
            view.showWonGameToast();
        }
    }

    private void sendWonGame() {
        if (dataTransitor != null) {
            dataTransitor.sendData(new TransitorData(TransitorData.FINISH, null));
        }
    }

    private void sendLostGame() {
        if (dataTransitor != null) {
            dataTransitor.sendData(new TransitorData(TransitorData.FINISH_LOST, null));
        }
    }

    @Override
    public void wrongAnswer() {
        view.currentQuestionWrongBackground(currentQuestion);
        view.setNullListenerForButton(currentQuestion);
        sendLostGame();
    }

    @Override
    public void errorQuestion() {
        view.showSimpleError();
    }

    @Override
    public void configCurrentButton() {
        view.setListenerForButton(currentQuestion);
    }

    @Override
    public void onClickCurrentQuestion() {
        QuestionForGame question = getQuestion(currentQuestion + 1);
        if (question != null) {
            view.startQuestionActivity(question);
        } else {
            view.showSimpleError();
        }

    }

    @Override
    public void backPressed() {
        IDataTransitorCommunication.setInstance(null);
        dataTransitor = null;
        view.finish();
    }

    private boolean checkIfWon() {
        return currentQuestion == NUMBER_OF_QUESTIONS;
    }

    public int getCurrentQuestion() {
        return currentQuestion;
    }

    @Nullable
    public QuestionForGame getQuestion(@IntRange(from = 1, to = 10) final int question) {

        if (question >= 1 && question <= 3) {
            return calcQuestion(LEVEL_1);
        } else if (question >= 4 && question <= 6) {
            return calcQuestion(LEVEL_2);
        } else if (question >= 7 && question <= 9) {
            return calcQuestion(LEVEL_3);
        } else if (question == 10) {
            return calcQuestion(LEVEL_4);
        }
        return null;
    }

    @Nullable
    private QuestionForGame calcQuestion(@IntRange(from = 1, to = 4) final int level) {
        List<QuestionForGame> questionForGames = questionsController.getQuestionsOfLevel(level);

        if (isEmpty(questionForGames)) {
            return null;
        }

        Collections.shuffle(questionForGames);

        //Checks if question was already token
        for (QuestionForGame questionForGame : questionForGames) {
            int questionID = questionForGame.question.getId();
            if (!questionsTaken.contains(questionID)) {
                questionsTaken.add(questionID);
                return questionForGame;
            }
        }
        //If all question were already token, returns first one by default
        return questionForGames.get(0);
    }

    @Override
    public void gotUpdate(@NonNull Object data) {
        if (data instanceof TransitorData) {
            TransitorData transitorData = (TransitorData) data;
            if (transitorData.type == TransitorData.FINISH) {
                view.showLostGameToast();
            } else if (transitorData.type == TransitorData.FINISH_LOST) {
                view.showWonGameToast();
            }
        }
    }

    @Override
    public void gotError(Throwable throwable) {
        view.showConnectionError();
    }
}
