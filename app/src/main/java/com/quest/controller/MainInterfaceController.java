package com.quest.controller;

import android.support.annotation.NonNull;

public class MainInterfaceController implements MainInterfaceContract.Controller{

    private String mCountry;

    private MainInterfaceContract.View view;

    public MainInterfaceController(@NonNull MainInterfaceContract.View view) {
        this.view = view;
    }

    @Override
    public void onClickTestKnowledgeButton() {
        view.startAllQuestionsActivity(mCountry);
    }

    @Override
    public void onClickChallengeButton() {
        view.startChallengeActivity(mCountry);
    }

    @Override
    public void onClickReceiveChallengeButton() {
        view.startReceiveChallengeActivity();
    }

    @Override
    public void onClickBluetoothButton() {
        view.startBluetoothActivity();
    }

    @Override
    public void onGotCountry(String country) {
        mCountry = country;
    }
}
