package com.quest.controller;

import com.quest.model.Answer;
import com.quest.model.Question;

import java.io.Serializable;

public class QuestionForGame implements Serializable{

    private static final long serialVersionUID = 158375107579072814L;

    public Question question;
    public Answer answer1;
    public Answer answer2;
    public Answer answer3;
    public Answer answer4;


    QuestionForGame(){}

    QuestionForGame(final Question question)  {

        this.question = question;
        this.answer1 = question.getAnswers().get(0);
        this.answer2 = question.getAnswers().get(1);
        this.answer3 = question.getAnswers().get(2);
        this.answer4 = question.getAnswers().get(3);
    }

}
