package com.quest.controller;

import java.util.LinkedList;
import java.util.List;

final class QuestionSingleton extends LinkedList<QuestionForGame>{
    private static QuestionSingleton instance;

    private QuestionSingleton(){}

    static List<QuestionForGame> getInstance(){
        if(instance == null) {
            instance = new QuestionSingleton();
        }
        return instance;
    }
}
