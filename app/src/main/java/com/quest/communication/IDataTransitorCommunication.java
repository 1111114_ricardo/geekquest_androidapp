package com.quest.communication;

import android.support.annotation.Nullable;

public final class IDataTransitorCommunication {

    @Nullable
    private static IDataTransitor instance;

    private IDataTransitorCommunication() {
    }

    public static void setInstance(@Nullable IDataTransitor dataTransitor) {
        instance = dataTransitor;
    }

    @Nullable
    public static IDataTransitor getInstance() {
        return instance;
    }
}
