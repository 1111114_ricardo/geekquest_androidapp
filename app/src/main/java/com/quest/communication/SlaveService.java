package com.quest.communication;

import android.support.annotation.Nullable;

import java.util.List;

public interface SlaveService extends IDataTransitor{

    @Nullable
    public List<String> getConnections();

    public void connect(final String connection);

    public void disconnect();

    public void onError(Throwable throwable);
}
