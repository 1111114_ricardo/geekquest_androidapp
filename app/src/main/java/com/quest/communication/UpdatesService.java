package com.quest.communication;

import android.support.annotation.NonNull;

public interface UpdatesService {

    public void gotUpdate(@NonNull Object data);

    public void gotError(Throwable throwable);

}
