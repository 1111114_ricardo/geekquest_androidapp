package com.quest.communication;

public interface MasterService extends IDataTransitor{

    public void listen();

    public void stopListen();

    public void onError(Throwable throwable);

}
