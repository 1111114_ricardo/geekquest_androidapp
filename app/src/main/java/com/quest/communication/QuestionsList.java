package com.quest.communication;

import com.quest.controller.QuestionForGame;

import java.util.ArrayList;
import java.util.Collection;

public class QuestionsList extends ArrayList<QuestionForGame> {

    private static final long serialVersionUID = -7596595410416162208L;

    public QuestionsList(int initialCapacity) {
        super(initialCapacity);
    }

    public QuestionsList() {
    }

    public QuestionsList(Collection<? extends QuestionForGame> c) {
        super(c);
    }
}
