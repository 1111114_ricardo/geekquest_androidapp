package com.quest.communication.sockets;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.quest.communication.ConnectionService;
import com.quest.communication.SlaveService;
import com.quest.communication.UpdatesService;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SocketSlave implements SlaveService {

    private static final String TAG = SocketSlave.class.getName();

    private static final String BROADCAST_ADDRESS = "255.255.255.255";

    private List<String> connections;

    private Handler handler;

    @Nullable
    private GetAvailableConnectionsThread getConnectionsThread;

    @Nullable
    private ConnectAndListenThread connectAndListenThread;

    private ConnectionService connectionService;

    @Nullable
    private UpdatesService updatesService;

    @Nullable
    private ObjectOutputStream objectOutputStream;

    public SocketSlave(@NonNull ConnectionService connectionService, @NonNull UpdatesService updatesService) {
        this(connectionService);
        this.updatesService = updatesService;
    }

    public SocketSlave(@NonNull ConnectionService connectionService) {
        handler = new Handler(Looper.getMainLooper());
        connections = new ArrayList<>();
        this.connectionService = connectionService;
    }

    @Nullable
    @Override
    public List<String> getConnections() {
        if (getConnectionsThread == null) {
            getConnectionsThread = new GetAvailableConnectionsThread();
            getConnectionsThread.start();
        }
        return connections;
    }

    @Override
    public void connect(final String connection) {
        destroyGetConnectionsThread();

        if (connectAndListenThread == null) {
            connectAndListenThread = new ConnectAndListenThread(connection);
            connectAndListenThread.start();
        }
    }

    private void destroyGetConnectionsThread() {
        if (getConnectionsThread != null) {
            getConnectionsThread.interrupt();
            getConnectionsThread.closeSocket();
            getConnectionsThread = null;
        }
    }

    private void destroyConnectAndListenThread() {
        if (connectAndListenThread != null) {
            connectAndListenThread.interrupt();
            connectAndListenThread.closeSocket();
            connectAndListenThread = null;
        }
    }

    @Override
    public void disconnect() {
        destroyGetConnectionsThread();
        destroyConnectAndListenThread();
    }

    private void onSocketOpened(final ObjectOutputStream objectOutputStream) {
        this.objectOutputStream = objectOutputStream;
        this.connectionService.onConnectionOpen();
    }

    @Override
    public void onError(Throwable throwable) {
        if (updatesService != null) {
            updatesService.gotError(throwable);
        }
    }

    @Override
    public void sendData(final Object obj) {
        if (objectOutputStream != null) {
            Observable.create(new Observable.OnSubscribe<Void>() {
                @Override
                public void call(Subscriber<? super Void> subscriber) {
                    try {
                        objectOutputStream.writeObject(obj);
                        subscriber.onNext(null);
                    } catch (IOException e) {
                        subscriber.onError(e);
                    }
                    subscriber.onCompleted();
                }
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Void>() {
                        @Override
                        public void onCompleted() {
                            //Intentional blank method
                        }

                        @Override
                        public void onError(Throwable e) {
                            //Intentional blank method
                        }

                        @Override
                        public void onNext(Void aVoid) {
                            //Intentional blank method
                        }
                    });
        }
    }

    @Override
    public void onDataReceived(Object obj) {
        if (updatesService != null) {
            updatesService.gotUpdate(obj);
        }
    }

    @Override
    public void setUpdatesService(@Nullable UpdatesService updatesService) {
        this.updatesService = updatesService;
    }

    private void updateConnections(@NonNull String connection) {
        connections.add(connection);
        connectionService.onNewConnection(connection);
    }

    private class ConnectAndListenThread extends Thread {

        private boolean requestToClose;

        private String connection;

        @Nullable
        private Socket socket;

        public ConnectAndListenThread(String connection) {
            this.connection = connection;
        }

        @Override
        public void run() {
            try {
                socket = new Socket(connection, SocketMaster.SERVER_PORT);
                Log.d(TAG, ">>>Server accepted! Socket Open:");
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());

                transmitSocketOpened(objectOutputStream);

                while (!this.isInterrupted() && socket.isConnected()) {
                    Log.d(TAG, ">>>Listening stream for input");
                    Object object = objectInputStream.readObject();
                    transmitNewData(object);
                    Log.d(TAG, ">>>Got new data");
                }

                objectInputStream.close();
                objectOutputStream.close();
                socket.close();
            } catch (IOException | ClassNotFoundException e) {
                Log.e(TAG, ">>>Got exception: ", e);
                transmitErrorOccurred(e);
            }
        }

        private void transmitNewData(@Nullable final Object object) {
            if (object != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        onDataReceived(object);
                    }
                });
            }
        }

        private void transmitSocketOpened(final ObjectOutputStream objectOutputStream) {
            if (!requestToClose) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        onSocketOpened(objectOutputStream);
                    }
                });
            }
        }

        private void transmitErrorOccurred(final Throwable throwable) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    onError(throwable);
                }
            });
        }

        public void closeSocket() {
            requestToClose = true;
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing socket", e);
                }
            }
        }
    }

    private class GetAvailableConnectionsThread extends Thread {

        private boolean requestToClose;

        private DatagramSocket datagramSocket;

        @Override
        public void run() {

            // Find the server using UDP broadcast
            try {
                //Open a random port to send the package
                datagramSocket = new DatagramSocket();
                datagramSocket.setBroadcast(true);

                byte[] sendData = SocketMaster.DISCOVER_MESSAGE.getBytes(SocketMaster.DEFAULT_ENCODING);

                //Try the 255.255.255.255 first
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length,
                        InetAddress.getByName(BROADCAST_ADDRESS), SocketMaster.DISCOVER_PORT);
                datagramSocket.send(sendPacket);
                Log.d(TAG, ">>> Request packet sent to: 255.255.255.255 (DEFAULT)");

                // Broadcast the message over all the network interfaces
                Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
                while (interfaces.hasMoreElements()) {
                    NetworkInterface networkInterface = (NetworkInterface) interfaces.nextElement();

                    if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                        continue; // Don't want to broadcast to the loopback interface
                    }

                    for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
                        InetAddress broadcast = interfaceAddress.getBroadcast();
                        if (broadcast == null) {
                            continue;
                        }

                        // Send the broadcast package!
                        DatagramPacket sendPacketThroughInterface = new DatagramPacket(sendData, sendData.length, broadcast, SocketMaster.DISCOVER_PORT);
                        datagramSocket.send(sendPacketThroughInterface);
                        Log.d(TAG, ">>> Request packet sent to: " + broadcast.getHostAddress() + "; Interface: " + networkInterface.getDisplayName());

                    }
                }

                Log.d(TAG, ">>> Done looping over all network interfaces. Now waiting for a reply!");

                while (!this.isInterrupted()) {
                    //Wait for a response
                    byte[] recvBuf = new byte[15000];
                    DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
                    datagramSocket.receive(receivePacket);

                    //We have a response
                    Log.d(TAG, ">>> Broadcast response from server: " + receivePacket.getAddress().getHostAddress());

                    //Check if the message is correct
                    String message = new String(receivePacket.getData(), SocketMaster.DEFAULT_ENCODING).trim();
                    if (message.equals(SocketMaster.DISCOVER_RESPONSE)) {
                        transmitNewConnection(receivePacket.getAddress().getHostAddress());
                    }
                }

                //Close the port!
                datagramSocket.close();
            } catch (IOException ex) {
                transmitErrorOccurred(ex);
            }
        }

        private void transmitNewConnection(@NonNull final String connection) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    updateConnections(connection);
                }
            });
        }

        private void transmitErrorOccurred(final Throwable throwable) {
            if (!requestToClose) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        onError(throwable);
                    }
                });
            }
        }

        public void closeSocket() {
            requestToClose = true;
            datagramSocket.close();
        }
    }
}
