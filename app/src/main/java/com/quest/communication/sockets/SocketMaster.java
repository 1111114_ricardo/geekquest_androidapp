package com.quest.communication.sockets;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.quest.communication.MasterService;
import com.quest.communication.UpdatesService;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SocketMaster implements MasterService {

    static final int SERVER_PORT = 8080;

    static final int DISCOVER_PORT = 8888;

    static final String DISCOVER_MESSAGE = "DISCOVER_FUIFSERVER_REQUEST";

    static final String DISCOVER_RESPONSE = "DISCOVER_FUIFSERVER_RESPONSE";

    private static final String DISCOVER_IP = "0.0.0.0";

    static final String DEFAULT_ENCODING = "UTF-8";

    private static final String TAG = SocketMaster.class.getName();

    private Handler handler;

    @Nullable
    private UpdatesService updatesService;

    @Nullable
    private IncomingDataThread incomingData;

    @Nullable
    private DiscoverableThread discoverableThread;

    @Nullable
    private ObjectOutputStream objectOutputStream;

    public SocketMaster(@NonNull UpdatesService updatesService) {
        this();
        this.updatesService = updatesService;
    }

    public SocketMaster() {
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void listen() {
        incomingData = new IncomingDataThread();
        incomingData.start();
        discoverableThread = new DiscoverableThread();
        discoverableThread.start();
    }

    @Override
    public void stopListen() {
        if (incomingData != null) {
            incomingData.interrupt();
            incomingData.closeSocket();
            incomingData = null;
        }
        stopBeingDiscoverable();
    }

    public void stopBeingDiscoverable() {
        if (discoverableThread != null) {
            discoverableThread.interrupt();
            discoverableThread.closeSocket();
            discoverableThread = null;
        }
    }

    @Override
    public void onError(Throwable throwable) {
        if (updatesService != null) {
            updatesService.gotError(throwable);
        }
        stopListen();
    }

    @Override
    public void sendData(@NonNull final Object obj) {
        if (objectOutputStream != null) {
            Observable.create(new Observable.OnSubscribe<Void>() {
                @Override
                public void call(Subscriber<? super Void> subscriber) {
                    try {
                        objectOutputStream.writeObject(obj);
                        subscriber.onNext(null);
                    } catch (IOException e) {
                        subscriber.onError(e);
                    }
                    subscriber.onCompleted();
                }
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Void>() {
                        @Override
                        public void onCompleted() {
                            //Intentional blank method
                        }

                        @Override
                        public void onError(Throwable e) {
                            //Intentional blank method
                        }

                        @Override
                        public void onNext(Void aVoid) {
                            //Intentional blank method
                        }
                    });
        }
    }

    private void onSocketOpened(final ObjectOutputStream objectOutputStream) {
        this.objectOutputStream = objectOutputStream;
    }

    @Override
    public void onDataReceived(Object obj) {
        if (updatesService != null) {
            updatesService.gotUpdate(obj);
        }
    }

    @Override
    public void setUpdatesService(@Nullable UpdatesService updatesService) {
        this.updatesService = updatesService;
    }

    private class IncomingDataThread extends Thread {

        private boolean requestToClose;
        private ServerSocket serverSocket;

        @Nullable
        private Socket client;

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket(SERVER_PORT);
                client = serverSocket.accept();
                Log.d(TAG, ">>>Socket connected: " + client.getInetAddress().getHostAddress());
                //Once gets the socket, closes UDP broadcast
                transmitInterruptRequest();

                ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream());
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(client.getOutputStream());
                transmitSocketOpened(objectOutputStream);

                while (!this.isInterrupted() && client.isConnected()) {
                    Log.d(TAG, ">>>Listening stream for input");
                    final Object objectRead = objectInputStream.readObject();
                    transmitData(objectRead);
                    Log.d(TAG, ">>>Got new data");
                }

                objectInputStream.close();
                objectOutputStream.close();
                client.close();
                serverSocket.close();
            } catch (final IOException | ClassNotFoundException e) {
                Log.e(TAG, ">>> Got exception: ", e);
                transmitError(e);
            }

        }

        private void transmitSocketOpened(final ObjectOutputStream objectOutputStream) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    onSocketOpened(objectOutputStream);
                }
            });
        }

        private void transmitData(final Object data) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    onDataReceived(data);
                }
            });
        }

        private void transmitError(final Throwable throwable) {
            if (!requestToClose) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        onError(throwable);
                    }
                });
            }
        }

        private void transmitInterruptRequest() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    stopBeingDiscoverable();
                }
            });
        }

        public void closeSocket() {
            requestToClose = true;
            try {
                if (client != null) {
                    client.close();
                }
                serverSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Error closing socket", e);
            }
        }
    }

    private class DiscoverableThread extends Thread {

        private boolean requestToClose;

        private DatagramSocket socket;

        @Override
        public void run() {
            try {
                //Keep a socket open to listen to all the UDP traffic that is destined for this port
                socket = new DatagramSocket(DISCOVER_PORT, InetAddress.getByName(DISCOVER_IP));
                socket.setBroadcast(true);

                while (!this.isInterrupted()) {
                    Log.d(TAG, ">>>Ready to receive broadcast packets!");
                    //Receive a packet
                    byte[] recvBuf = new byte[15000];
                    DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
                    socket.receive(packet);

                    //Packet received
                    String message = new String(packet.getData(), DEFAULT_ENCODING).trim();
                    Log.d(TAG, ">>>Discovery packet received from: " + packet.getAddress().getHostAddress());
                    Log.d(TAG, ">>>Packet received; data: " + message);

                    //See if the packet holds the right command (message)

                    if (message.equals(DISCOVER_MESSAGE)) {
                        byte[] sendData = DISCOVER_RESPONSE.getBytes(DEFAULT_ENCODING);

                        //Send a response
                        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, packet.getAddress(), packet.getPort());
                        socket.send(sendPacket);

                        Log.d(TAG, ">>>Sent packet to: " + sendPacket.getAddress().getHostAddress());
                    }
                }
            } catch (final IOException ex) {
                Log.e(TAG, ">>>Got exception: ", ex);
                if (!requestToClose) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            onError(ex);
                        }
                    });
                }
            }
        }

        public void closeSocket() {
            requestToClose = true;
            socket.close();
        }
    }
}
