package com.quest.communication;

import android.support.annotation.IntDef;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class TransitorData implements Serializable {

    private static final long serialVersionUID = -8814776723393910525L;

    @IntDef({TransitorData.QUESTIONS, TransitorData.FINISH, TransitorData.FINISH_LOST})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }

    public final static int QUESTIONS = 0;
    public final static int FINISH = 1;
    public final static int FINISH_LOST = 2;

    public
    @TransitorData.Type
    int type;
    public Object data;

    public TransitorData(@Type int type, Object data) {
        this.type = type;
        this.data = data;
    }
}
