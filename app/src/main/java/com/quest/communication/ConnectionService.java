package com.quest.communication;

import android.support.annotation.NonNull;

public interface ConnectionService {

    public void onNewConnection(@NonNull final String connection);

    public void onConnectionOpen();

}
