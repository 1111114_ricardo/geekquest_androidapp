package com.quest.communication;

import android.support.annotation.Nullable;

public interface IDataTransitor {

    public void sendData(Object obj);
    public void onDataReceived(Object obj);

    public void setUpdatesService(@Nullable UpdatesService updatesService);
}
