package com.quest.request;

import android.support.annotation.NonNull;

import com.quest.model.Question;

import java.util.List;

public interface ApiClientService {

    interface Response<T>{
        void onSuccess(T ob);
        void onError(Throwable t);
    }

    void getSetOfQuestions(@NonNull final Response<List<Question>> callback, @NonNull String region);

}
