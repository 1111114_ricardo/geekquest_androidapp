package com.quest.request;

public final class Constants {
    public static final String DESCRIPTION = "Description";
    public static final String DIFFICULTY = "Difficulty";
    public static final String CORRECT = "Correct";
    public static final String ID = "ID";
    public static final String ANSWERS = "Answers";

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT = "Accept";
    public static final String APLLICATION_JSON = "application/json";
}
