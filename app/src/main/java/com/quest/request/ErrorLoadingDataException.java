package com.quest.request;

public class ErrorLoadingDataException extends Exception {
    public ErrorLoadingDataException(String s) {
        super(s);
    }
}
