package com.quest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Answer implements Serializable{

    private static final long serialVersionUID = 8776477694523174485L;

    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Correct")
    @Expose
    private Boolean correct;

    /**
     *
     * @return
     * The iD
     */
    public Integer getID() {
        return iD;
    }

    /**
     *
     * @param iD
     * The ID
     */
    public void setID(Integer iD) {
        this.iD = iD;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The correct
     */
    public Boolean isCorrect() {
        return correct;
    }

    /**
     *
     * @param correct
     * The Correct
     */
    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }
}
