package com.quest.model;

import java.io.Serializable;

public class AnswerForQuestion implements Serializable{
    private Question question;
    private Answer answer;
    private boolean correct;

    public AnswerForQuestion(Question question, Answer answer, boolean isCorrect){
        this.setQuestion(question);
        this.setAnswer(answer);
        this.setCorrect(isCorrect);
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean isCorrect) {
        this.correct = isCorrect;
    }
}
