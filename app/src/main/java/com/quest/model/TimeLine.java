package com.quest.model;

import java.util.Date;

public class TimeLine {
    private String description;
    private String name;
    private Date date;

    public TimeLine(String description, String name, Date date){
        this.setDescription(description);
        this.setName(name);
        this.setDate(date);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
