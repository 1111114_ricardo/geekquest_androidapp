package com.quest.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.quest.R;
import com.quest.controller.BeChallengedContract;
import com.quest.controller.BeChallengedController;
import com.quest.controller.QuestionForGame;

import java.util.List;

public class ReceiveChallengeActivity extends AppCompatActivity implements BeChallengedContract.View {

    private BeChallengedContract.Controller controller;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        controller = new BeChallengedController(this);
        controller.listenForChallenges();
    }

    @Override
    protected void onResume() {
        super.onResume();
        controller.viewCompletelyLoaded();
    }

    @Override
    public void startAllQuestionsActivity(@NonNull List<QuestionForGame> questions) {
        Intent intent = new Intent(this, AllQuestionsActivity.class);
        intent.putExtra(AllQuestionsActivity.MULTIPLAYER, true);
        startActivity(intent);
    }

    @Override
    public void showConnectionError() {
        Toast.makeText(this, R.string.error_making_connection, Toast.LENGTH_LONG).show();
    }

    @Override
    public void end() {
        finish();
    }

    @Override
    public void onBackPressed() {
        controller.gameFinished();
    }
}
