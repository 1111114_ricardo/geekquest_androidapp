package com.quest.view;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.quest.R;
import com.quest.communication.bluetooth.ConnectThread;
import com.quest.communication.bluetooth.ConnectedThread;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Set;

public class BluetoothSelectorActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final int SUCCESS_CONNECT = 0;
    public static final int MESSAGE_READ = 1;
    public static final int MESSAGE_WRITE = 2;
    public static final int MESSAGE_TOAST = 3;

    public static BluetoothAdapter btAdapter;

    public static Handler mHandler;
    public static ConnectThread connect;
    public static ConnectedThread connectedThread;

    Button searchDevicesButton;
    ArrayAdapter<String> listAdapter;
    ListView listView;
    Set<BluetoothDevice> devicesArray;
    ArrayList<String> pairedDevices;
    ArrayList<BluetoothDevice> devices;
    IntentFilter filter;
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_selector);
        init();
        if (btAdapter == null) {
            Toast.makeText(getApplicationContext(), R.string.bluetooth_undetected, Toast.LENGTH_SHORT).show();
            finish();
        } else {
            if (!btAdapter.isEnabled()) {
                turnOnBT();
            }

            getPairedDevices();
            startDiscovery();
        }

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case SUCCESS_CONNECT:
                        connectedThread = new ConnectedThread((BluetoothSocket) msg.obj);
                        Toast.makeText(getApplicationContext(), "Connect", Toast.LENGTH_SHORT).show();
                        String s = "Successfully connected";
                        connectedThread.write(s.getBytes(Charset.forName("UTF-8")));
                        break;
                    case MESSAGE_READ:
                        byte[] readBuff = (byte[]) msg.obj;
                        String string = new String(readBuff, Charset.forName("UTF-8"));
                        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
    }

    private void startDiscovery() {
        btAdapter.cancelDiscovery();
        btAdapter.startDiscovery();
    }

    private void turnOnBT() {
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(intent, 1);
    }

    private void init() {
        searchDevicesButton = (Button) findViewById(R.id.searchDevicesBtn);
        searchDevicesButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                startDiscovery();
            }
        });

        listView = (ListView) findViewById(R.id.btListView);
        listView.setOnItemClickListener(this);
        listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, 0);
        listView.setAdapter(listAdapter);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        pairedDevices = new ArrayList<String>();
        filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        devices = new ArrayList<BluetoothDevice>();
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    devices.add(device);
                    String s = "";
                    for (int a = 0; a < pairedDevices.size(); a++) {
                        if (device.getName().equals(pairedDevices.get(a))) {
                            s = "(" + getString(R.string.paired) + ")";
                            break;
                        }
                    }

                    listAdapter.add(device.getName() + " " + s + "\n" + device.getAddress());
                } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action) && btAdapter.getState() == btAdapter.STATE_OFF) {
                    turnOnBT();
                }
            }
        };

        registerReceiver(receiver, filter);
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        registerReceiver(receiver, filter);
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(receiver, filter);
        filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(receiver, filter);
    }

    private void getPairedDevices() {
        devicesArray = btAdapter.getBondedDevices();
        if (!devicesArray.isEmpty()) {
            for (BluetoothDevice device : devicesArray) {
                pairedDevices.add(device.getName());
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), R.string.bluetooth_disabled, Toast.LENGTH_SHORT).show();
            connectedThread.cancel();
            connect.cancel();
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

        if (btAdapter.isDiscovering()) {
            btAdapter.cancelDiscovery();
        }
        BluetoothDevice selectedDevice = devices.get(arg2);
        if (!pairedDevices.contains(selectedDevice.getName())) {
            if (selectedDevice.createBond()) {
                pairedDevices.add(selectedDevice.getName());
            } else {
                Toast.makeText(getApplicationContext(), R.string.error_pairing, Toast.LENGTH_SHORT).show();
            }
        }
        connect = new ConnectThread(selectedDevice);
        connect.start();
        Toast.makeText(getApplicationContext(), getString(R.string.btConnected) + " " + selectedDevice.getName(), Toast.LENGTH_SHORT).show();
    }
}
