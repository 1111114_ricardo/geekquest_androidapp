package com.quest.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.quest.R;
import com.quest.controller.QuestionContract;
import com.quest.controller.QuestionController;
import com.quest.controller.QuestionForGame;

public class QuestionActivity extends AppCompatActivity implements QuestionContract.View {

    private View.OnClickListener onRightAnswerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            controller.onRightAnswer();
        }
    };

    private View.OnClickListener onWrongAnswerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            controller.onWrongAnswer();
        }
    };

    private static final int PICK_CONTACT = 12345;
    private static final int PHONE_CONTACT = 12354;

    private QuestionContract.Controller controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_layout);
        controller = new QuestionController(this);
        controller.onCreateView(getIntent().getExtras());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.question_activity_action_bar, menu);
        if(AllQuestionsActivity.getUsedHalfQuestions()) {
            menu.findItem(R.id.half_of_questions).setVisible(false);
        }
        if(AllQuestionsActivity.getUsedPhone()) {
            menu.findItem(R.id.phone).setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.half_of_questions:
                controller.onHalfQuestionsClick();
                item.setVisible(false);
                return true;
            case R.id.phone:
                controller.onPhoneClick();
                item.setVisible(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void startContactPicker() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (reqCode == PICK_CONTACT ) {
            if( resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                Cursor c = getContentResolver().query(contactData, null, null, null, null);
                //TODO find other way of solving
                if (Utils.checkNotNull(c).moveToFirst()) {
                    String phone = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    controller.onPhoneNumberGotten(phone);
                }
                c.close();
            } else {
                controller.onUnexpectedError();
            }
        }
    }

    public void makePhoneCall(@NonNull final String phoneNumber) {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);

        if(permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Intent phoneIntent = new Intent(Intent.ACTION_CALL);
            phoneIntent.setData(Uri.parse("tel:" + phoneNumber));
            startActivityForResult(phoneIntent, PHONE_CONTACT);
        }
        //TODO implement else condition
    }

    public void setHalfOfQuestions(@NonNull QuestionForGame question) {
        int count = 0;
        if(!question.answer1.isCorrect()){
            count++;
            findViewById(R.id.answer1Button).setVisibility(View.GONE);
        }
        if(!question.answer2.isCorrect() && count<2){
            count++;
            findViewById(R.id.answer2Button).setVisibility(View.GONE);
        }
        if(!question.answer3.isCorrect() && count<2){
            count++;
            findViewById(R.id.answer3Button).setVisibility(View.GONE);
        }
        if(!question.answer4.isCorrect() && count<2){
            findViewById(R.id.answer4Button).setVisibility(View.GONE);
        }
    }

    public void configLayout(@NonNull QuestionForGame question) {
        ((TextView)findViewById(R.id.questionTextView)).setText(question.question.getDescription());

        Button answer1 = (Button)findViewById(R.id.answer1Button);
        answer1.setText(question.answer1.getDescription());
        answer1.setOnClickListener(question.answer1.isCorrect() ? onRightAnswerListener : onWrongAnswerListener);

        Button answer2 = (Button)findViewById(R.id.answer2Button);
        answer2.setText(question.answer2.getDescription());
        answer2.setOnClickListener(question.answer2.isCorrect() ? onRightAnswerListener : onWrongAnswerListener);

        Button answer3 = (Button)findViewById(R.id.answer3Button);
        answer3.setText(question.answer3.getDescription());
        answer3.setOnClickListener(question.answer3.isCorrect() ? onRightAnswerListener : onWrongAnswerListener);

        Button answer4 = (Button)findViewById(R.id.answer4Button);
        answer4.setText(question.answer4.getDescription());
        answer4.setOnClickListener(question.answer4.isCorrect() ? onRightAnswerListener : onWrongAnswerListener);
    }

    @Override
    public void showUnexpectedErrorDialog() {
        Toast.makeText(this, R.string.unexpected_error_performing_operation_label, Toast.LENGTH_LONG).show();
    }

    @Override
    public void sendResultRight() {
        Intent resultIntent = new Intent();
        setResult(AllQuestionsActivity.RESULT_RIGHT, resultIntent);
        finish();
    }

    @Override
    public void sendResultWrong() {
        Intent resultIntent = new Intent();
        setResult(AllQuestionsActivity.RESULT_WRONG, resultIntent);
        finish();
    }

    @Override
    public void sendQuestionError() {
        Intent resultIntent = new Intent();
        setResult(AllQuestionsActivity.RESULT_ERROR, resultIntent);
        finish();
    }
}
