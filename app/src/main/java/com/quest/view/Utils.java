package com.quest.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.quest.R;

import java.util.Collection;

public final class Utils {
    private Utils(){}

    static ProgressDialog getDownloadingQuestionsDialog(Activity activity){
        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(activity.getResources().getString(R.string.downloading_questions));
        return progressDialog;
    }

    static ProgressDialog getSuggestingQuestionDialog(Activity activity){
        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(activity.getResources().getString(R.string.sending_question));
        return progressDialog;
    }

    static ProgressDialog getSuggestedQuestionsDialog(Activity activity){
        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(activity.getResources().getString(R.string.downloading_suggested_questions));
        return progressDialog;
    }

    static ProgressDialog getVoteQuestionDialog(Activity activity){
        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(activity.getResources().getString(R.string.voting_question));
        return progressDialog;
    }

    @NonNull
    public static <S> S checkNotNull(@Nullable S object, @Nullable String message){
        if(object == null){
            throw new IllegalArgumentException(message);
        }
        return object;
    }

    @NonNull
    public static <S> S checkNotNull(@Nullable S object){
        return checkNotNull(object, null);
    }

    public static boolean isNotEmpty(@Nullable String stringToEvaluate){
        return stringToEvaluate != null && !stringToEvaluate.equals("");
    }

    public static boolean isEmpty(@Nullable Collection collection){
        return collection == null || collection.isEmpty();
    }
}
