package com.quest.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.quest.R;
import com.quest.controller.AllQuestionsController;
import com.quest.controller.KnowledgeContract;
import com.quest.controller.QuestionForGame;

import java.util.ArrayList;
import java.util.List;

public class AllQuestionsActivity extends AppCompatActivity implements KnowledgeContract.View{

    public static final int QUESTION_RESULT = 65535; //Max for 16-bit number
    public static final String QUESTION_BUNDLE = "QuestionBundle";
    public static final String MULTIPLAYER = "Multiplayer";
    public static final int RESULT_RIGHT = 1;
    public static final int RESULT_WRONG = 0;
    public static final int RESULT_ERROR = -1;

    private static boolean usedHalfQuestions;
    private static boolean usedPhone;

    private List<Button> buttons;

    private ProgressDialog progressDialog;

    private KnowledgeContract.Controller controller;

    @Nullable
    private Toast toast;

    public static void setUsedHalfQuestions(boolean usedHalfQuestions) {
        AllQuestionsActivity.usedHalfQuestions = usedHalfQuestions;
    }

    public static void setUsedPhone(boolean usedPhone) {
        AllQuestionsActivity.usedPhone = usedPhone;
    }

    public static boolean getUsedHalfQuestions() {
        return usedHalfQuestions;
    }

    public static boolean getUsedPhone() {
        return usedPhone;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_questions_layout);
        setUsedHalfQuestions(false);
        setUsedPhone(false);
        buttons = new ArrayList<>();

        addButtons((ViewGroup) findViewById(R.id.all_questions_linear_layout));

        setTextForButtons();

        controller = new AllQuestionsController(this);
        controller.onCreateView(this.getIntent().getExtras());

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(toast != null){
            toast.cancel();
        }
    }

    private void setTextForButtons() {
        String[] questions = getResources().getStringArray(R.array.questions);
        for(int i = 0;i<questions.length;i++){
            buttons.get(i).setText(questions[i]);
        }
    }

    private void addButtons(ViewGroup viewGroup) {
        int viewGroupChildCount = viewGroup.getChildCount();
        for (int i=0; i<viewGroupChildCount; i++) {
            View child = viewGroup.getChildAt(i);
            if(child instanceof ViewGroup) {
                addButtons(((ViewGroup) child));
            }
            if(child instanceof Button){
                buttons.add((Button)child);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == QUESTION_RESULT){
            if (resultCode == RESULT_RIGHT) {
                controller.rightAnswer();
            } else if(resultCode == RESULT_WRONG){
                controller.wrongAnswer();
            } else if(resultCode == RESULT_ERROR){
                controller.errorQuestion();
            }
        }
    }

    @Override
    public void showLoadingQuestionsDialog() {
        progressDialog = Utils.getDownloadingQuestionsDialog(AllQuestionsActivity.this);
        progressDialog.show();
    }

    @Override
    public void dismissLoadingQuestionsDialog() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showToastErrorLoadingQuestions() {
        toast = Toast.makeText(this,getResources().getText(R.string.error_try_again),Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void setListenerForButton(final int currentQuestion) {
        buttons.get(currentQuestion).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.onClickCurrentQuestion();
            }
        });
    }

    @Override
    public void setNullListenerForButton(int currentQuestion) {
        Button button = buttons.get(currentQuestion);

        button.setOnClickListener(null);
    }

    @Override
    public void startQuestionActivity(@NonNull QuestionForGame question) {
        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra(QUESTION_BUNDLE, question);
        startActivityForResult(intent, QUESTION_RESULT);
    }

    @Override
    public void currentQuestionRightBackground(int currentQuestion) {
        Button button = buttons.get(currentQuestion);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            button.setBackground(getResources().getDrawable(R.drawable.right_wrong_button));
        }
//        button.setBackgroundColor(Color.GREEN);
        //TODO do else condition
        button.setEnabled(true);
    }

    @Override
    public void currentQuestionWrongBackground(int currentQuestion) {
        Button button = buttons.get(currentQuestion);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            button.setBackground(getResources().getDrawable(R.drawable.right_wrong_button));
        }
        //TODO do else condition
        button.setEnabled(false);
    }

    @Override
    public void showWonGameToast() {
        toast = Toast.makeText(this, R.string.won_knowledge_game,Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showLostGameToast() {
        toast = Toast.makeText(this, R.string.lost_knowledge_game, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showSimpleError() {
        toast = Toast.makeText(this,R.string.result_error,Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showConnectionError() {
        toast = Toast.makeText(this, R.string.multiplayer_connection_error, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        controller.backPressed();
    }
}
