package com.quest.view;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.leakcanary.LeakCanary;

public class MyApplication extends Application{
    private static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        setContext(this);

        if (!LeakCanary.isInAnalyzerProcess(this)) {
            // The Analyzer process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            LeakCanary.install(this);
        }

    }

    @NonNull
    public static Context getContext(){
        return instance;
    }

    private static void setContext(@NonNull MyApplication application){
        instance = application;
    }
}
