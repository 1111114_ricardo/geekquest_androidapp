package com.quest.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.quest.R;
import com.quest.controller.ChallengeContract;
import com.quest.controller.ChallengeController;
import com.quest.controller.QuestionForGame;
import com.quest.controller.adapter.ConnectionsAdapter;

import java.util.ArrayList;
import java.util.List;

public class ChallengeActivity extends AppCompatActivity implements ChallengeContract.View, ConnectionsAdapter.OnItemClick {

    private ChallengeContract.Controller challengeController;

    private RecyclerView recyclerView;

    private ConnectionsAdapter connectionsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.challenge_layout);
        challengeController = new ChallengeController(this);

        challengeController.onBuildView(getIntent().getExtras());
    }

    @Override
    protected void onResume() {
        super.onResume();
        challengeController.onViewCompletelyLoaded();
    }

    @Override
    public void startAllQuestionsActivity(@NonNull List<QuestionForGame> questions) {
        Intent intent = new Intent(this, AllQuestionsActivity.class);
        intent.putExtra(AllQuestionsActivity.MULTIPLAYER, true);
        startActivity(intent);
    }

    @Override
    public void gotNewConnection(@NonNull String connection) {
        connectionsAdapter.addConnection(connection);
        connectionsAdapter.notifyDataSetChanged();
    }

    @Override
    public void buildRecyclerView(@Nullable List<String> connections) {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_challenges);

        connectionsAdapter = new ConnectionsAdapter(connections != null ? connections : new ArrayList<String>(), this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(connectionsAdapter);
    }

    @Override
    public void showConnectionError() {
        Toast.makeText(this, R.string.error_making_connection, Toast.LENGTH_LONG).show();
    }

    @Override
    public void end() {
        finish();
    }

    @Override
    public void onItemClicked(int position, String connection) {
        challengeController.connect(connection);
    }
}
