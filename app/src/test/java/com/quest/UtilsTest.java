package com.quest;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

import static com.quest.view.Utils.checkNotNull;
import static com.quest.view.Utils.isEmpty;
import static com.quest.view.Utils.isNotEmpty;

public class UtilsTest {

    @Test
    public void nullCollectionAndIsEmptyReturnsTrue(){
        Assert.assertEquals(true, isEmpty(null));
    }

    @Test
    public void emptyCollectionAndIsEmptyReturnsTrue(){
        Assert.assertEquals(true, isEmpty(new ArrayList()));
    }

    @Test
    public void collectionWithElementsAndIsEmptyReturnsFalse(){
        ArrayList<Object> list = new ArrayList<>();
        list.add(new Object());
        Assert.assertEquals(false, isEmpty(list));
    }

    @Test
    public void nullStringAndIsNotEmptyReturnsFalse(){
        Assert.assertEquals(false, isNotEmpty(null));
    }

    @Test
    public void emptyStringAndIsNotEmptyReturnsFalse(){
        Assert.assertEquals(false, isNotEmpty(""));
    }

    @Test
    public void notEmptyStringAndIsNotEmptyReturnsFalse(){
        Assert.assertEquals(true, isNotEmpty("test"));
    }

    @Test(expected=IllegalArgumentException.class)
    public void nullParameterAndCheckNotNullThrowsException(){
        checkNotNull(null);
    }

    @Test
    public void notNullParameterAndCheckNotNullReturnsObject(){
        Assert.assertNotNull(checkNotNull(new Object()));
    }
}
