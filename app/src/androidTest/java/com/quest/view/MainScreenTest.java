package com.quest.view;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.quest.R;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainScreenTest {

    @Rule
    public ActivityTestRule<MainActivity> mainTestRule =
            new ActivityTestRule<>(MainActivity.class, true, false);

    @Before
    public void init(){
        Intent intent = new Intent();
        intent.putExtra(MainActivity.LOCATION_DISABLED, true);
        mainTestRule.launchActivity(intent);
    }

    @Ignore
    @Test
    public void clickTestKnowledgeButtonAndOpensGame(){
        onView(withText(R.string.test_knowledge)).perform(click());

        onView(withId(R.id.all_questions_linear_layout)).check(matches(isDisplayed()));
    }
}
