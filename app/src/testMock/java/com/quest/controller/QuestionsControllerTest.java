package com.quest.controller;

import com.quest.request.ApiClientService;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class QuestionsControllerTest {

    private QuestionsController controller;

    @Before
    public void init(){
        controller = new QuestionsController();
        controller.getQuestions(new ApiClientService.Response<List<QuestionForGame>>() {
            @Override
            public void onSuccess(List<QuestionForGame> ob) {
                //No need to perform action
            }

            @Override
            public void onError(Throwable t) {
                //No need to perform action
            }
        }, "Portugal");
    }

    @Test
    public void getQuestionsOfSpecificLevel(){
        int specificLevel = 2;

        List<QuestionForGame> questions = controller.getQuestionsOfLevel(specificLevel);
        Assert.assertNotNull(questions);

        for(QuestionForGame q : questions){
            Assert.assertEquals(true, q.question.getDifficulty() == specificLevel);
        }
    }
}
