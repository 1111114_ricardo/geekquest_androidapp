package com.quest.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class MainInterfaceControllerTest {

    @Mock
    private MainInterfaceContract.View view;

    private MainInterfaceContract.Controller controller;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);

        controller = new MainInterfaceController(view);
    }

    @Test
    public void clicksTestKnowledgeButtonAndOpensAllQuestionsActivity(){
        controller.onGotCountry("Portugal");
        controller.onClickTestKnowledgeButton();
        verify(view).startAllQuestionsActivity("Portugal");
    }

}
