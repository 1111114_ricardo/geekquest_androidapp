package com.quest.controller;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.quest.view.Utils.checkNotNull;
import static org.mockito.Mockito.verify;

public class AllQuestionsControllerTest {

    @Mock
    private KnowledgeContract.View view;

    private KnowledgeContract.Controller controller;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);

        controller = new AllQuestionsController(view);
    }

    @Test
    public void getQuestionsAndDismissesDialog(){
        controller.getQuestions();
        verify(view).showLoadingQuestionsDialog();
        verify(view).dismissLoadingQuestionsDialog();
    }

    @Test
    public void correctAnswerAndConfigsNextQuestion(){
        int currentQuestion = ((AllQuestionsController)controller).getCurrentQuestion();
        controller.rightAnswer();
        verify(view).currentQuestionRightBackground(currentQuestion);
        verify(view).setListenerForButton(++currentQuestion);
    }

    @Test
    public void correctAnswerAndWinsTheGame(){
        for(int i = 0; i<(AllQuestionsController.NUMBER_OF_QUESTIONS-1);i++){
            correctAnswerAndConfigsNextQuestion();
        }
        controller.rightAnswer();
        verify(view).showWonGameToast();
    }

    @Test
    public void clicksCurrentQuestionAndOpensItsView(){
        controller.getQuestions();
        controller.onClickCurrentQuestion();
        ArgumentCaptor<QuestionForGame> peopleCaptor = ArgumentCaptor.forClass(QuestionForGame.class);
        verify(view).startQuestionActivity(peopleCaptor.capture());
        Assert.assertNotNull(peopleCaptor.getValue());
    }

    @Test
    public void getQuestionLevel1(){
        AllQuestionsController controller = (AllQuestionsController) this.controller;
        controller.getQuestions();
        final QuestionForGame question1 = controller.getQuestion(1);
        final QuestionForGame question2 = controller.getQuestion(2);
        final QuestionForGame question3 = controller.getQuestion(3);

        Assert.assertEquals(true, checkNotNull(question1).question.getDifficulty() == AllQuestionsController.LEVEL_1 &&
                checkNotNull(question2).question.getDifficulty() == AllQuestionsController.LEVEL_1 &&
                checkNotNull(question3).question.getDifficulty() == AllQuestionsController.LEVEL_1);
    }

    @Test
    public void getQuestionLevel2(){
        AllQuestionsController controller = (AllQuestionsController) this.controller;
        controller.getQuestions();
        final QuestionForGame question1 = controller.getQuestion(4);
        final QuestionForGame question2 = controller.getQuestion(5);
        final QuestionForGame question3 = controller.getQuestion(6);

        Assert.assertEquals(true, checkNotNull(question1).question.getDifficulty() == AllQuestionsController.LEVEL_2 &&
                checkNotNull(question2).question.getDifficulty() == AllQuestionsController.LEVEL_2 &&
                checkNotNull(question3).question.getDifficulty() == AllQuestionsController.LEVEL_2);
    }

    @Test
    public void getQuestionLevel3(){
        AllQuestionsController controller = (AllQuestionsController) this.controller;
        controller.getQuestions();
        final QuestionForGame question1 = controller.getQuestion(7);
        final QuestionForGame question2 = controller.getQuestion(8);
        final QuestionForGame question3 = controller.getQuestion(9);

        Assert.assertEquals(true, checkNotNull(question1).question.getDifficulty() == AllQuestionsController.LEVEL_3 &&
                checkNotNull(question2).question.getDifficulty() == AllQuestionsController.LEVEL_3 &&
                checkNotNull(question3).question.getDifficulty() == AllQuestionsController.LEVEL_3);
    }

    @Test
    public void getQuestionLevel4(){
        AllQuestionsController controller = (AllQuestionsController) this.controller;
        controller.getQuestions();
        final QuestionForGame question1 = controller.getQuestion(10);

        Assert.assertEquals(true, checkNotNull(question1).question.getDifficulty() == AllQuestionsController.LEVEL_4 );
    }
}
