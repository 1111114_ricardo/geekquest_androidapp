package com.quest.controller;

import android.os.Bundle;

import com.quest.request.ApiClientService;
import com.quest.view.AllQuestionsActivity;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static com.quest.view.Utils.checkNotNull;
import static junit.framework.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class QuestionControllerTest {

    @Mock
    private QuestionContract.View view;

    private QuestionContract.Controller controller;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);

        controller = new QuestionController(view);
    }

    @Test
    public void gotPhoneNumberAndMakesPhoneCall(){
        String stringToTest = "123456789";
        controller.onPhoneNumberGotten(stringToTest);
        verify(view).makePhoneCall(stringToTest);
    }

    @Test
    public void gotWrongPhoneNumberAndShowsError(){
        controller.onPhoneNumberGotten(null);
        verify(view).showUnexpectedErrorDialog();
    }

    @Test
    public void onPhoneClickAndOpensContactPicker(){
        controller.onPhoneClick();
        verify(view).startContactPicker();
    }

    @Test
    public void onCreateViewAndInvalidBundle(){
        controller.onCreateView(null);
        verify(view).sendQuestionError();
    }

    @Test
    public void onCreateViewAndConfigsView(){
        final QuestionsController questionsController = new QuestionsController();
        questionsController.getQuestions(new ApiClientService.Response<List<QuestionForGame>>() {
            @Override
            public void onSuccess(List<QuestionForGame> ob) {
                final Bundle bundle = mock(Bundle.class);
                final QuestionForGame question = checkNotNull(questionsController.getQuestionsOfLevel(AllQuestionsController.LEVEL_1)).get(0);
                when(bundle.getSerializable(AllQuestionsActivity.QUESTION_BUNDLE)).thenReturn(question);
                controller.onCreateView(bundle);

                verify(view).configLayout(question);
            }

            @Override
            public void onError(Throwable t) {
                fail("Error: "+ t);
            }
        }, "Portugal");

    }

}
