[![Build Status](https://travis-ci.org/RicardoMiguel/GeekQuest_AndroidApp.svg?branch=master)](https://travis-ci.org/RicardoMiguel/GeekQuest_AndroidApp)
[![codecov](https://codecov.io/gh/RicardoMiguel/GeekQuest_AndroidApp/branch/master/graph/badge.svg)](https://codecov.io/gh/RicardoMiguel/GeekQuest_AndroidApp)
[![license](https://img.shields.io/github/license/mashape/apistatus.svg)]()

### GeekQuest

The application is a game like "Who wants to be a millionaire" in which a user can play locally.

The user can also challenge a player who is close by, either over Local Area Network or Bluetooth.


## Setup

### 1 - Install Android Studio (Tested on version 2.2.2)
### 2 - Clone repository
Optional: Name project directory 'GeekQuest_AndroidApp' for better compatibility with the IDE.

### 3 - Install project dependencies
### 4 - Choose Build Variant

The 'mock' flavour uses stubbed injection data while the 'prod' flavour gets the data from a server.
The 'release' build is not signed so it won't work.

### 5 - Click 'Run' to install the application in a device.


## Unit Tests

You can run all the tests directly from IDE but you can also run them from terminal:

### 1 - Open terminal and go to project directory

### 2 - Run the command
```
gradlew check --stacktrace
```

This command will compile all necessary files in order to run unit tests and also lint check, in all flavours.


## Instrumentation Tests

As unit testing, you can run them directly from IDE. In order to run them from terminal:

### 1 - Open terminal and go to project directory

### 2 - Run the command
```
gradlew connectedMockDebugAndroidTest --stacktrace
```

This command will compile all necessary files and search for a device to run the tests.

## PMD

### 1 - Open terminal and go to project directory

### 2 - Run the command
```
gradlew pmd
```

## FindBugs

### 1 - Open terminal and go to project directory

### 2 - Run the command
```
gradlew findbugs
```



